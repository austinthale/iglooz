﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirePitHitbox : Hitbox {
    public override void ResolveHit(snowBallCollision snowBall) {
			// Debug.Log(name);
      GetComponentInParent<FireManager>().Cleanup();
    }
}
