﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour {
  public delegate void OnUseItem(Vector3 position, Quaternion rotation);
	private OnUseItem useItem;
	public string useItemInput = "Use Item";
	public GameObject inventoryBox;

    public GameObject spawnLocation;
    public float respawnTime = 1f;
    public AudioSource splash;

    // Use this for initialization
    void Start() {

	}
	
	// Update is called once per frame
	void Update() {
        if (Input.GetButtonDown(useItemInput) && useItem != null) {
			    useItem(transform.localPosition, transform.localRotation);
			    var image = inventoryBox.GetComponent<RawImage>();
			    image.texture = null;
			    image.color = new Color(1f, 1f, 1f, 0f);
			    useItem = null;
        }
	}

	public void SetOnItemUse(OnUseItem onUseItem) {
		useItem = onUseItem;
	}

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        //print("collided with " + other.gameObject.name);
        if (hit.gameObject.tag == "Water")
        {
            splash.Play();
            // print("collided with " + other.gameObject.name);
            StartCoroutine(InitiateRespawn());
        }
    }

    private IEnumerator InitiateRespawn()
    {
        int prevLayer = gameObject.layer;
        gameObject.layer = LayerMask.NameToLayer("NoCollision");

        PlayerHealth health = GetComponent<PlayerHealth>();
        Transform camTransform = transform.GetComponentInChildren<Camera>().transform;
        Vector3 camPos = camTransform.localPosition;
        Quaternion camRot = camTransform.localRotation;
        camTransform.parent = null;

        yield return new WaitForSeconds(respawnTime);

        health.isFrozen = false;
        health.invulnerable = true;
        transform.position = spawnLocation.transform.position;
        transform.rotation = spawnLocation.transform.rotation;
        gameObject.layer = prevLayer;

        camTransform.parent = transform;
        camTransform.localPosition = camPos;
        camTransform.localRotation = camRot;

        int blinkNum = 10;

        Renderer[] renderers = transform.GetComponentsInChildren<Renderer>();
        while (blinkNum > 0)
        {
            yield return new WaitForSeconds(.1f);
            foreach (var r in renderers)
                r.enabled = false;
            yield return new WaitForSeconds(.2f);
            foreach (var r in renderers)
                r.enabled = true;
            blinkNum--;
        }
        health.invulnerable = false;
    }
}
