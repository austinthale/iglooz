﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;


public class PlayerHealth : MonoBehaviour
{
    public float startingHealth = 100f;
    public float currentHealth;
    public Slider healthSlider;
    public Image damageImage;
    public float flashSpeed = 5f;
    public Color flashColour = new Color(0f, 0f, 1f, 0.1f);
    public float meltTime = 5f;
    public GameObject iceCube;
    public float frozenTime = 5f;
    public bool invulnerable = false;

    //Animator anim;
    //AudioSource playerAudio;
    public bool isFrozen;
    bool damaged;

	//Audio variables
	public AudioClip hitSound;
	public AudioClip freezeSound;
	public AudioClip steamSound;
	private AudioSource source;
	private float volLowRange = 0.5f;
	private float volHighRange = 1.0f;

    void Awake()
    {
        //anim = GetComponent<Animator>();
        //playerAudio = GetComponent<AudioSource>();
        currentHealth = startingHealth;

		source = GetComponent<AudioSource> ();
    }


    void Update()
    {
        if (damaged)
        {
            damageImage.color = flashColour;
        }
        else
        {
            damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        damaged = false;
    }


    public void TakeDamage(float amount)
    {
        if (!isFrozen && !invulnerable)
        {
            damaged = true;

            currentHealth -= amount;

            healthSlider.value = currentHealth;

            //playerAudio.Play();

			if (currentHealth <= 0f && !isFrozen) {
				Freeze ();

				float vol = Random.Range (volLowRange, volHighRange);
				source.PlayOneShot(freezeSound, vol);
			} else {
				float vol = Random.Range (volLowRange, volHighRange);
				source.PlayOneShot(hitSound, vol);
			}
        }
        
    }

    public void Heal(float amount)
    {
        float sumHealth = currentHealth + amount;
        currentHealth = (sumHealth > startingHealth) ? startingHealth : sumHealth;
        healthSlider.value = (int)currentHealth;
    }


    void Freeze()
    {
        isFrozen = true;
        iceCube.SetActive(true);

        //GameOverManager.Instance.GameOver();

        StartCoroutine(UnFreeze());
    }

    private IEnumerator UnFreeze()
    {
        float startTime = Time.time;
        yield return new WaitUntil(() => Time.time - startTime >= frozenTime || !isFrozen);

        startTime = Time.time;
        while (Time.time - startTime < meltTime && isFrozen)
        {
            float t = (Time.time - startTime) / meltTime;
            currentHealth = (int) Mathf.Lerp(0f, startingHealth, t);
            healthSlider.value = currentHealth;
            iceCube.transform.localScale = new Vector3(1f-t/2, 1f - t, 1f-t/2); // change each 1f to 2f for larger model
            yield return null;
        }

        currentHealth = startingHealth;
        healthSlider.value = currentHealth;

		float vol = Random.Range (volLowRange, volHighRange);
		source.PlayOneShot(steamSound, vol);

        isFrozen = false;
        iceCube.transform.localScale = new Vector3(1, 1, 1);
        iceCube.SetActive(false);
    }


    public void RestartLevel()
    {
        SceneManager.LoadScene(0);
    }
}