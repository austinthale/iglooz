﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


public class IglooHitbox : Hitbox
{
    private IglooHealth igloo;
    public float healthRegen = 5f;

	public AudioClip iglooHealSound;
	private AudioSource source;
	private float volLowRange = 0.5f;
	private float volHighRange = 1.0f;

    private void Awake()
    {
        igloo = GetComponentInParent<IglooHealth>();

		source = GetComponent<AudioSource> ();
    }

    public override void ResolveHit(snowBallCollision snowBall)
    {
        if (snowBall.owner == igloo.owner)
        {
            igloo.Health += (snowBall.damage > 25) ? 10f : healthRegen;

			float vol = Random.Range (volLowRange, volHighRange);
			source.PlayOneShot(iglooHealSound, vol);
        }
    }

    public IglooHealth GetIgloo()
    {
        return igloo;
    }

}
