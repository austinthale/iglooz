﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Hitbox : MonoBehaviour {
    public abstract void ResolveHit(snowBallCollision snowBall);
}
