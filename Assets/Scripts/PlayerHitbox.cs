﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHitbox : Hitbox {
    public override void ResolveHit(snowBallCollision snowBall)
    {
        GetComponent<PlayerHealth>().TakeDamage((int)snowBall.damage);
        // print("Current health: " + GetComponent<PlayerHealth>().currentHealth);
    }
}
