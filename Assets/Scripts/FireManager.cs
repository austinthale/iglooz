﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class FireManager : MonoBehaviour {
	private IglooHealth nearbyIgloo;
	public int timeToLive = 15;
	public float meltSpeedModifier = 1.5f;

	//Audio variables
	public AudioClip fireSound;
	private AudioSource source;
	private float volLowRange = 0.5f;
	private float volHighRange = 1.0f;

	// Use this for initialization
	IEnumerator Start() {
		source = GetComponent<AudioSource> ();
		source.PlayOneShot (fireSound, 0.3f);
		yield return StartCoroutine("BurnThenDie");
		if (nearbyIgloo) StopIglooModifier();
		Destroy(gameObject);
	}

	public void Cleanup() {
		StopCoroutine("BurnThenDie");
		if (nearbyIgloo) StopIglooModifier();

		source.Stop();

		Destroy(gameObject);
	}

	void OnTriggerStay(Collider other) {
		if (other.gameObject.CompareTag("Player")) {
			other.GetComponent<PlayerHealth>().Heal(5f * Time.deltaTime);
		} else if (!nearbyIgloo && other.gameObject.CompareTag("Igloo")) {
			nearbyIgloo = other.GetComponent<IglooHitbox>().GetIgloo();
			nearbyIgloo.healthLossPerSecond *= meltSpeedModifier;
		}
		float vol = Random.Range (volLowRange, volHighRange);
	}

	void StopIglooModifier() {
		nearbyIgloo.healthLossPerSecond /= meltSpeedModifier;
	}
	
	IEnumerator BurnThenDie() {
		// suspend execution for 5 seconds
		yield return new WaitForSeconds(timeToLive);
  }
}
