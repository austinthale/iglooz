﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class Timer : MonoBehaviour
{

    public GameOverManager gameOverManager;
    public IglooHealth IglooHealth;
    public Transform cameraTransform;
    private Camera cam;
    public int time;
    public Text timerText;
    public RectTransform pointerTransform;
    public Transform timerLocation;
    public float distFromScreenCenterRatio = 0.8f;
    public float timerTextOffset = 20;

    // Use this for initialization
    void Start()
    {
        timerText.text = "";//((IglooHealth.Health / IglooHealth.healthLossPerSecond).ToString("D2"));
        StartCoroutine(Countdown());
        cam = cameraTransform.GetComponent<Camera>();
    }

    void LateUpdate()
    {
        Vector3 pixelPos = cam.WorldToScreenPoint(timerLocation.position);
        Vector2 pixelPosFlat = pixelPos;
        float screenSize = Mathf.Min(cam.pixelRect.width, cam.pixelRect.height);
        float allowedRadius = screenSize * distFromScreenCenterRatio / 2f;
        Vector2 cameraCenter = cam.pixelRect.center;

        if (pixelPos.z > 0 && Vector2.Distance(pixelPosFlat, cameraCenter) < allowedRadius)
        {
            transform.position = pixelPosFlat;
            timerText.transform.localPosition = Vector2.up * timerTextOffset;
            pointerTransform.gameObject.SetActive(false);
        }
        else
        {
            //Outside of view
            if (pixelPos.z < 0)
            {
                pixelPosFlat *= -1;
            }

            float angle = Vector2.SignedAngle(Vector2.up, pixelPosFlat - cameraCenter);
            //print(angle);
            Quaternion rot = Quaternion.Euler(0, 0, angle);
            Vector2 offset = rot * (Vector2.up * allowedRadius);
            transform.position = cameraCenter + offset;

            pointerTransform.gameObject.SetActive(true);
            pointerTransform.localRotation = rot;

            timerText.transform.localPosition = rot * (Vector2.up * -timerTextOffset);
        }
    }
    //private string ConvertSeconds()
    //{
    //    int minutes = time / 60;
    //    int seconds = time % 60;
    //    return minutes + ":" + seconds.ToString("D2");
    //}

    IEnumerator Countdown()
    {
        do
        {
            if (timerLocation.position.y > 6)
                transform.position = timerLocation.position;
            time = (int)(IglooHealth.Health / IglooHealth.healthLossPerSecond);
            yield return new WaitForSeconds(0.3f);
            timerText.text = "00:" + time.ToString("D2");

            if (time > 10)
                timerText.color = Color.green;
            if (time <= 10)
                timerText.color = Color.red;
            if (time <= 0)
                gameOverManager.GameOver();

        } while (time > 0);
    }
}