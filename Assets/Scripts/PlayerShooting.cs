﻿using UnityEngine;
using System.Collections;

public class PlayerShooting : MonoBehaviour
{
    //public GameObject projectile;
    public float speed = 50f;
    public float timeBtwShots = 0.3f;
    private float timeLastFired;
    public GameObject snowBallPrefab;
    public Transform snowBallSpawn;
    public string shoot = "Fire1";
    private PlayerHealth healthScript;
    private bool largeSnowballsEnabled = false;

    //Audio variables
    public AudioClip throwSound;
    private AudioSource source;
	private float volLowRange = 0.5f;
	private float volHighRange = 1.0f;

    private void Start()
    {
		source = GetComponent<AudioSource> ();
        timeLastFired = -timeBtwShots;
        healthScript = GetComponent<PlayerHealth>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton(shoot) && (Time.time - timeLastFired) >= timeBtwShots && healthScript.isFrozen == false)
        {
            timeLastFired = Time.time;

			float vol = Random.Range (volLowRange, volHighRange);
			source.PlayOneShot(throwSound, vol);

            Shoot();
        }
    }

    void Shoot()
    {
        var snowBall = (GameObject)Instantiate(snowBallPrefab, snowBallSpawn.position, snowBallSpawn.rotation);
        snowBall.GetComponent<Rigidbody>().velocity = snowBall.transform.forward * speed;
        snowBall.GetComponent<snowBallCollision>().owner = transform;
        if (largeSnowballsEnabled) 
        {
            snowBall.GetComponent<snowBallCollision>().damage = 50f;
            snowBall.transform.localScale *= 2;
        }
    }

    public void EnableBigSnowballs()
    {
        StartCoroutine("StartBigSnowballs");
    }

    public void EnableRapidFire()
    {
        StartCoroutine("StartRapidFire");
    }

    IEnumerator StartRapidFire()
    {
        timeBtwShots /= 5f;
        yield return new WaitForSeconds(15);
        timeBtwShots *= 5f;
    }

    IEnumerator StartBigSnowballs()
    {
        speed *= 1.2f;
        timeBtwShots *= 1.2f;
        largeSnowballsEnabled = true;
	    yield return new WaitForSeconds(15);
        speed /= 1.2f;
        timeBtwShots /= 1.2f;
        largeSnowballsEnabled = false;
    }
}
