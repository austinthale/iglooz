﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IglooHealth : MonoBehaviour {

    public float iglooHealth = 100f;
    public float maxHealth = 100f;
    public Transform owner;
    public float healthLossPerSecond = 200f / 60f;
    public Transform timerText;

    public float Health {
        get { return iglooHealth; }
        set { iglooHealth = Mathf.Clamp(value, 0, maxHealth);
            if (iglooHealth == 0)
                GameOverManager.Instance.GameOver();
        }
    }

    private void OnEnable()
    {
        StartCoroutine(Melt());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private IEnumerator Melt()
    {
        float lastUpdate = Time.time;
        yield return new WaitForSeconds(0.01f);
        while (true)
        {
            transform.localScale = new Vector3(1f, (iglooHealth / maxHealth),  1f);
            float deltaTime = Time.time - lastUpdate;
            lastUpdate = Time.time;
            Health -= healthLossPerSecond * deltaTime;
            yield return new WaitForSeconds(0.01f);
            //print(iglooHealth);
        }
    }
}
