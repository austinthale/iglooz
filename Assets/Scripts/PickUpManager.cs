﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUpManager : MonoBehaviour
{
    public enum PickUpTypes{ Fire, BigSnowballs, RapidFire, SpeedBoost };
    public PickUpTypes pickupType;
	private PlayerManager.OnUseItem onItemUse;
	public GameObject prefab;
	private GameObject player;
	public Texture fireTexture;
	public Texture bigSnowballTexture;
    public Texture rapidFireTexture;
    public Texture speedBoostTexture;

	private Texture texture;
    public float respawnTime = 1f;

	//Audio variables
	public AudioClip pickupSound;
	private AudioSource source;
	private float volLowRange = 0.5f;
	private float volHighRange = 1.0f;

	void Start()
	{
        EnablePickup();

		source = GetComponent<AudioSource> ();
    }

    void EnablePickup()
    {
        //print("Test");
        GetComponent<Renderer>().enabled = true;
        GetComponent<Collider>().enabled = true;

        System.Array types = System.Enum.GetValues(typeof(PickUpTypes));
        pickupType = (PickUpTypes)types.GetValue(Random.Range(0, types.Length));
        Renderer rend = GetComponent<Renderer>();
        switch (pickupType)
        {
            case PickUpTypes.Fire:
                onItemUse = UseFire;
                texture = fireTexture;
                rend.material.color = new Color(1f, 0.014084507f, 0.014084507f, 0.5f);
                break;
            case PickUpTypes.BigSnowballs:
                onItemUse = BigSnowballs;
                texture = bigSnowballTexture;
                rend.material.color = new Color(0.014084507f, 0.014084507f, 1f, 0.5f);
                break;
            case PickUpTypes.RapidFire:
                onItemUse = RapidFire;
                texture = rapidFireTexture;
                rend.material.color = new Color(0, 1f, 0, .5f);
                break;
            case PickUpTypes.SpeedBoost:
                onItemUse = SpeedBoost;
                texture = speedBoostTexture;
                rend.material.color = new Color(1f, 0, .5f, .5f);
                break;
            default:
                onItemUse = UseDefaultItem;
                break;
        }

    }

    void DisablePickup()
    {
        GetComponent<Renderer>().enabled = false;
        GetComponent<Collider>().enabled = false;
    }

	// Update is called once per frame
	void FixedUpdate()
	{
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
	}
	
  private void OnTriggerEnter(Collider other)
  {
    if (other.gameObject.CompareTag("Player"))
    	{
			player = other.gameObject;
			var playerManager = other.GetComponent<PlayerManager>();
			playerManager.SetOnItemUse(onItemUse);
			var image = playerManager.inventoryBox.GetComponent<RawImage>();
			image.texture = texture;
			image.color = new Color(1f, 1f, 1f, 1f);

			float vol = Random.Range (volLowRange, volHighRange);
			source.PlayOneShot(pickupSound, vol);

            //gameObject.SetActive(false);
            DisablePickup();
            StartCoroutine(respawnPowerUp(respawnTime));
    	}
    }
	
    private IEnumerator respawnPowerUp(float respawnTime)
    {
        //print("2nd test" + gameObject.activeInHierarchy);
        yield return new WaitForSeconds(respawnTime);
        //print("3rd test");

        EnablePickup();
    }

	public void UseFire(Vector3 characterPosition, Quaternion characterRotation)
	{
		Vector3 spawnPosition = new Vector3(characterPosition.x, characterPosition.y-(float)1.1, characterPosition.z + 1);
        var fire = (GameObject)Instantiate(prefab, spawnPosition, Quaternion.Euler(-90f, 0, 0));
	}
	
	public void BigSnowballs(Vector3 characterPosition, Quaternion characterRotation)
	{
		// Vector3 spawnPosition = new Vector3(characterPosition.x, 0.68f, characterPosition.z);
		player.GetComponent<PlayerShooting>().EnableBigSnowballs();
	}

    public void RapidFire(Vector3 characterPosition, Quaternion characterRotation)
    {
        player.GetComponent<PlayerShooting>().EnableRapidFire();
    }

    public void SpeedBoost(Vector3 characterPosition, Quaternion characterRotation)
    {
        player.GetComponent<PlayerMovement>().EnableSpeedBoost();
    }

    public void UseDefaultItem(Vector3 foo, Quaternion bar)
	{
		// Debug.Log("UseDefaultItem" + name);
	}
}
