﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


public class PlayerSlider : MonoBehaviour
{
    private new Collider collider;
    public float slideSpeed = 12f;
    private bool endSlide;

    //Audio variables
    public AudioSource pushSlide;
    public AudioSource slideSound;

	private float volLowRange = 0.5f;
	private float volHighRange = 1.0f;

    private void Start()
    {
        collider = GetComponent<Collider>();
		//sourcePush = GetComponent<AudioSource> ();
    }

    public void StartSlide(Vector3 slideDir)
    {
        collider.enabled = true;
        StopAllCoroutines();
        StartCoroutine(Slide(transform.InverseTransformVector(slideDir)));

		float vol = Random.Range (volLowRange, volHighRange);
        //source.PlayOneShot(pushSlide, vol);
        //source.PlayOneShot(slideSound, vol);
        pushSlide.Play();
        slideSound.Play();
    }

    IEnumerator Slide(Vector3 slideDir)
    {
        PlayerHealth health = GetComponentInParent<PlayerHealth>();
        endSlide = false;

        while (!endSlide && health.isFrozen)
        {
            transform.parent.Translate(slideDir * slideSpeed * Time.deltaTime);
            yield return null;
        }

        FinishSlide();
    }

    private void FinishSlide()
    {
        collider.enabled = false;
		slideSound.Stop ();
        endSlide = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        FinishSlide();


        
        
    }

    

}

