﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class snowBallCollision : MonoBehaviour {
    public Transform owner;
    public Transform enemy;
    public float damage = 25f;

    private void OnTriggerEnter(Collider other)
    {
        //print("snowball hit " + other.gameObject.name + " " + Time.time);
		if (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Igloo"))
        {
            if (!other.transform.IsChildOf(owner))
            {
                Hitbox hitbox = other.transform.GetComponent<Hitbox>();
                if (hitbox)
                    hitbox.ResolveHit(this);

                Destroy(gameObject);
                //gameObject.SetActive(false);
            }
        }
        else if (other.gameObject.CompareTag("Fire"))
        {
            Hitbox hitbox = other.transform.GetComponent<Hitbox>();
            if (hitbox) hitbox.ResolveHit(this);
        }
        else if (other.gameObject.CompareTag("Floor"))
        {
            Destroy(gameObject);
        }
    }
}
