﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileTrajectory : MonoBehaviour {

    public float offsetDist;
    public Vector3 defaultRelativePosition;
    public Transform offsetObj;
    public Transform startLocation;
    public float launchVelocity;
    public float projectionGravity;
	
	// Update is called once per frame
	void LateUpdate () {
        if (offsetObj)
        {
            RaycastHit hit;
            if (TrajectoryCast(out hit))
            {
                offsetObj.position = hit.point + hit.normal * offsetDist;
                offsetObj.LookAt(hit.point, Vector3.up);
            }
            else
            {
                offsetObj.position = transform.position + defaultRelativePosition;
                offsetObj.rotation = Quaternion.Euler(90, 0, 0);
            }
        }
	}

    public bool TrajectoryCast(out RaycastHit hit)
    {
        Vector3 oldPosition = startLocation.position;
        Vector3 newPosition = oldPosition;
        Vector3 velocity = startLocation.forward * launchVelocity;
        Vector3 gravVector = Vector3.down * projectionGravity;

        const float minCheckDist = 0.25f;

        const int maxSteps = 50;
        float timeDelta = 1f / velocity.magnitude;

        for (int i = 0; i < maxSteps; ++i)
        {
            newPosition += velocity * timeDelta + (0.5f * gravVector) * timeDelta * timeDelta;
            velocity += gravVector * timeDelta;

            if (Vector3.Distance(newPosition, oldPosition) > minCheckDist)
            {
                if (Physics.Linecast(oldPosition, newPosition, out hit, 1 << LayerMask.NameToLayer("Solid") | 1 << LayerMask.NameToLayer("Structure")))
                    return true;

                oldPosition = newPosition;
            }
        }

        hit = new RaycastHit();
        return false;
    }
}
