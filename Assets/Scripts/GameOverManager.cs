﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameOverManager : MonoBehaviour
{
    public GameObject player1;
    public GameObject player2;

    public GameObject Timer1;
    public GameObject Timer2;

    public IglooHealth p1IglooHealth;
    public IglooHealth p2IglooHealth;

    public GameObject gameOverPanel;
    public Text gameOverText;

    public float restartDelay = 5f;         // Time to wait before restarting the level


    //Animator anim;                          // Reference to the animator component.
    //float countdownRestart;                     // Timer to count up to restarting the level

	//Audio variables
	public AudioClip backgroundSound;
	public AudioClip lostSound;
	private AudioSource source;
	private float volLowRange = 0.5f;
	private float volHighRange = 1.0f;

    public static GameOverManager Instance { get; private set; }

    void Awake()
    {
        // Set up the reference.
        //anim = GetComponent<Animator>();
        gameOverText.text = "";
        gameOverPanel.SetActive(false);
        if (Instance == null)
        {
            Instance = this;
        }
		source = GetComponent<AudioSource> ();
    }


    private void ShowGameOver()
    {
        gameOverPanel.SetActive(true);

        //get difference timer of players
        //int healthTotal = player1.GetComponent<PlayerHealth>().currentHealth - player2.GetComponent<PlayerHealth>().currentHealth;
        //get difference between health and timer of players
        // float healthTotal = player1.GetComponent<PlayerHealth>().currentHealth - player2.GetComponent<PlayerHealth>().currentHealth;
        int timerTotal = Timer1.GetComponent<Timer>().time - Timer2.GetComponent<Timer>().time;

        if (/*healthTotal > 0 || */timerTotal > 0)
            gameOverText.text = "Player 2 is the IGLOOZER";
        else if (/*healthTotal < 0 || */timerTotal < 0)
            gameOverText.text = "Player 1 is the IGLOOZER";
        else if (/*healthTotal == 0 || */timerTotal == 0)
            gameOverText.text = "DRAW!!!";
        else
            gameOverText.text = "ERROR";

		//Game over sound
		float vol = Random.Range (volLowRange, volHighRange);
		source.PlayOneShot(lostSound, vol);
    }

    private void DisablePlayers()
    {
        player1.GetComponent<PlayerHealth>().enabled = false;
        player1.GetComponentInChildren<PlayerSlider>().enabled = false;
        player1.GetComponent<PlayerMovement>().enabled = false;
        player1.GetComponent<PlayerShooting>().enabled = false;
        player2.GetComponent<PlayerHealth>().enabled = false;
        player2.GetComponent<PlayerMovement>().enabled = false;
        player2.GetComponent<PlayerShooting>().enabled = false;
        player2.GetComponentInChildren<PlayerSlider>().enabled = false;
    }

    private void DisableMelting()
    {
        p1IglooHealth.enabled = false;
        p2IglooHealth.enabled = false;
    }

    public void GameOver()
    {
        DisablePlayers();
        DisableMelting();
        ShowGameOver();

        StartCoroutine(RestartAfterDelay());
    }

    private IEnumerator RestartAfterDelay()
    {
        yield return new WaitForSeconds(5f);
        SceneManager.LoadScene(0);
    }
}